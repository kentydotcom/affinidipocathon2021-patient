# Affinidi POCathon Health Insurance Use Case Implementation - Patient - Holder

## Introduction

Welcome to the Patient Portal for the Health Insurance Use Case Implementation designed by Dr Kent Lau [https://linkedin.com/in/kentglau].

<br/>
<br/>
<img src="./src/containers/masked_patient.png">